# Checaí

Um site para marcar coisas!

Eu amo marcar coisas, é uma das minhas atividades fúteis favoritas, criei esse site para funcionar com uma rotação semanal entre temas para você poder marcar quantas coisas sobre tal assunto você curte.

[![Netlify Status](https://api.netlify.com/api/v1/badges/8059dafd-eca2-46e1-9992-20d987d37359/deploy-status)](https://app.netlify.com/sites/checai/deploys)
